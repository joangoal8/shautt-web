const IMAGE_PATH = '/images';
const VIDEO_PATH = '/videos';

function getImageSrc(imageName) {
  return `${IMAGE_PATH}/${imageName}`;
}

function getVideoSrc(videoName) {
  return `${VIDEO_PATH}/${videoName}`;
}

export {
  getImageSrc,
  getVideoSrc,
};
