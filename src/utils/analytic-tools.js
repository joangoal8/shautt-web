import router from '@/router/index';

const USER_INTERACTION = 'USER_INTERACTION';
const WEB_HOME_EVENTS = 'WEB_HOME_EVENTS';
const homeEvents = (componentId) => {
  const event = {
    event_category: '',
    event_label: WEB_HOME_EVENTS,
    value: '',
  };
  let action = '';
  switch (componentId) {
    case 'appleStoreBtn':
      action = 'click_apple_store_button';
      event.event_category = USER_INTERACTION;
      event.value = true;
      break;
    case 'videoBtn':
      action = 'click_intro_video_button';
      event.event_category = USER_INTERACTION;
      event.value = true;
      break;
    case 'videoButtonScanner':
      action = 'click_scanner_section_video_button';
      event.event_category = USER_INTERACTION;
      event.value = true;
      break;
    default:
      return null;
  }
  return { action, event };
};

function getAnalyticEventData(componentId) {
  let events;
  switch (router.currentRoute.name) {
    case 'Home':
      events = homeEvents(componentId);
      break;
    default:
      break;
  }
  return events;
}

export {
  // eslint-disable-next-line import/prefer-default-export
  getAnalyticEventData,
};
