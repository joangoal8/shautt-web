import Vue from 'vue';
import VueI18n from 'vue-i18n';
import 'bootstrap/dist/css/bootstrap.css';
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
import translations from '@/translations';
import Vuex from 'vuex';
import getStore from '../../__mocks__/store/index';

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.use(VueI18n);
Vue.use(Vuex);

const i18n = new VueI18n({
  locale: 'es-ES',
  messages: translations,
});

export {
  i18n,
  getStore,
};
