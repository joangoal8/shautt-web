import 'bootstrap/dist/css/bootstrap.css';
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
import Vue from 'vue';
import VueCookies from 'vue-cookies';
import VueFacebookPixel from 'vue-analytics-facebook-pixel';
import VueI18n from 'vue-i18n';
import translations from '@/translations';
import App from './App.vue';
import router from './router';
import store from './store/index';
import { apiInstall } from './plugins/Api';
import utilsPlugin from './plugins/utils';
import { firebaseInstall } from './plugins/firebase';

Vue.use(VueI18n);
Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.use(apiInstall);
Vue.use(VueCookies);
Vue.use(VueFacebookPixel);
Vue.use(utilsPlugin);
Vue.use(firebaseInstall);
const i18n = new VueI18n({
  locale: 'es-ES',
  messages: translations,
});

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount('#app');
