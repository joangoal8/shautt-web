const getProductTypesFormatted = (shoppingBasket) => shoppingBasket.map((item) => ({
  productTypeName: item.name_es,
  quantity: item.quantity,
}));

const build = (postalCode, shoppingBasket) => {
  const productTypes = getProductTypesFormatted(shoppingBasket);
  return {
    calculateShoppingCartsRequest: {
      postalCode,
      productTypes,
    },
  };
};

export default build;
