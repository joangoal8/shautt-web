import Vue from 'vue';
import VueRouter from 'vue-router';
import VueGtag from 'vue-gtag';
import { firebase, getMeasurementId } from '@/plugins/firebase';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "home" */ '@/views/Home.vue'),
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue'),
  },
  {
    path: '/privacy-policy',
    name: 'PrivacyPolicy',
    component: () => import(/* webpackChunkName: "privacy-policy" */ '@/views/PrivacyPolicy.vue'),
  },
  {
    path: '/terms-conditions',
    name: 'TermsConditions',
    component: () => import(/* webpackChunkName: "terms-conditions" */ '@/views/TermsConditions.vue'),
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '@/views/login/Login.vue'),
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import(/* webpackChunkName: "register" */ '@/views/login/Register.vue'),
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: () => import(/* webpackChunkName: "dashboard" */ '@/views/dashboard/Dashboard.vue'),
    meta: {
      requiresAuth: true,
    },
    children: [
      {
        path: '/dashboard/shopping-list',
        name: 'Dashboard/ShoppingList',
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/dashboard/ShoppingList.vue'),
      },
      {
        path: '/dashboard/compare-markets',
        name: 'Dashboard/CompareMarkets',
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/dashboard/CompareMarkets.vue'),
      },
    ],
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach(async (to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    const isUserLogged = await firebase.getCurrentUser();
    if (isUserLogged) {
      next();
      return;
    }
    next('/login');
  } else {
    next();
  }
});

// Use GA on every page load
Vue.use(VueGtag, {
  config: { id: getMeasurementId() },
}, router);

export default router;
