const roundTo2 = (num) => +(`${Math.round(`${num}e+2`)}e-2`);

const checkShoppingProductsStoreIndex = (storeIndex, shoppingCarts) => storeIndex !== null
  && shoppingCarts !== undefined && shoppingCarts.length > storeIndex;

const getters = {
  isUpdatingShoppingList: (state) => state.isUpdatingShoppingList,
  shoppingList: (state) => state.shoppingList.map((item) => ({
    product: item.name_es,
    quantity: item.quantity,
    ...item,
  })),

  shoppingProducts: (state) => {
    const shoppingProducts = [];
    state.shoppingProductsTypes.map(
      (category) => category.items.map(
        (item) => shoppingProducts.push({
          ...item,
          categoryName: category.name,
        }),
      ),
    );
    return shoppingProducts;
  },
  shoppingProductsTypes: (state) => state.shoppingProductsTypes,
  shoppingCarts: (state) => state.shoppingCarts,
  shoppingCartProducts: (state) => (storeIndex) => (checkShoppingProductsStoreIndex(storeIndex,
    state.shoppingCarts)
    ? state.shoppingCarts[storeIndex].shoppingCartProducts
    : []),
  selectedShoppingCartProducts: (state) => (storeIndex) => {
    const selectedProducts = [];
    if (checkShoppingProductsStoreIndex(storeIndex, state.shoppingCarts)) {
      state.shoppingCarts[storeIndex].shoppingCartProducts.forEach((cart) => {
        const price = roundTo2(cart.quantity * cart.selectedProduct.price);
        selectedProducts.push({
          item: cart.selectedProduct.name,
          quantity: cart.quantity,
          price,
        });
      });
    }
    return selectedProducts;
  },
  shoppingCartTotalAmount: (state) => (storeIndex) => (checkShoppingProductsStoreIndex(storeIndex,
    state.shoppingCarts)
    ? state.shoppingCarts[storeIndex].totalAmount
    : ''),

  productByStoreShoppingCart: (state) => state.productByStoreShoppingCart,
  customShoppingCartTotalAmount: (state) => {
    let totalPrice = 0;
    state.productByStoreShoppingCart.providerProducts.forEach((product) => {
      totalPrice += (product.quantity * product.price);
    });
    return roundTo2(totalPrice);
  },
  storeProducts: (state) => state.storeProducts,
};

export default getters;
