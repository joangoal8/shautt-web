const getProductIndex = (productArray, product) => {
  let index = -1;
  productArray.forEach((item, idx) => {
    if (product.name === item.name) {
      index = idx;
    }
  });
  return index;
};

const recalculateStoresTotalAmount = (state) => {
  state.shoppingCarts.forEach((store) => {
    let totalAmount = 0;
    store.shoppingCartProducts.forEach((products) => {
      totalAmount += products.selectedProduct.price * products.quantity;
    });
    Object.assign(store, { totalAmount });
  });
};

const mutations = {
  CLEAR_DASHBOARD(state) {
    state.shoppingLis = [];
    state.shoppingCarts = [];
  },
  SET_SHOPPING_LIST(state, shoppingList) {
    state.shoppingList = shoppingList;
  },
  SET_SHOPPING_LIST_UPDATE_STATUS(state, status) {
    state.isUpdatingShoppingList = status || false;
  },
  EDIT_SHOPPING_LIST(state, { item, quantity }) {
    const idx = state.shoppingList.findIndex((i) => i.itemId === item.itemId);
    state.shoppingList[idx].quantity = quantity;
  },
  SET_SHOPPING_PRODUCT_TYPES(state, productTypes) {
    state.shoppingProductsTypes = productTypes;
  },
  SET_SHOPPING_BASKET_ID(state, shoppingBasketId) {
    state.shoppingBasketId = shoppingBasketId;
  },
  SET_SHOPPING_CARTS(state, shoppingCarts) {
    if (shoppingCarts && shoppingCarts.length > 0) {
      state.shoppingCarts = shoppingCarts.filter((cart) => cart.stores.length === 1);
    }
  },
  BUILD_SHOPPING_CART(state, storeIndex) {
    if (storeIndex !== null && state.shoppingCarts.length > storeIndex) {
      state.productByStoreShoppingCart.storeIndex = storeIndex;
      state.productByStoreShoppingCart.providerProducts.splice(0,
        state.productByStoreShoppingCart.providerProducts.length);
      state.shoppingCarts[storeIndex].shoppingCartProducts.forEach((cart) => {
        state.productByStoreShoppingCart.providerName = 'providerName';
        state.productByStoreShoppingCart.providerProducts.push({
          ...cart.selectedProduct,
          quantity: cart.quantity,
        });
      });
    }
    return state.productByStoreShoppingCart;
  },
  CHANGE_PRODUCT_FROM_STORE(state, { selectedProduct, oldItemId }) {
    const { storeId } = selectedProduct;
    const storeCart = state.shoppingCarts.find((cart) => cart.stores[0].id === storeId);
    storeCart.shoppingCartProducts.forEach((product, idx) => {
      if (product.selectedProduct.id === oldItemId) {
        Object.assign(storeCart.shoppingCartProducts[idx].selectedProduct, selectedProduct);
      }
    });
    recalculateStoresTotalAmount(state);
  },
  CHANGE_PRODUCT_QUANTITY(state, { selectedProduct, quantity }) {
    const { storeId, id } = selectedProduct;
    const storeCart = state.shoppingCarts.find((cart) => cart.stores[0].id === storeId);
    storeCart.shoppingCartProducts.forEach((product, idx) => {
      if (product.selectedProduct.id === id) {
        storeCart.shoppingCartProducts[idx].quantity = quantity;
      }
    });
    recalculateStoresTotalAmount(state);
  },
  RECALCULATE_STORES_TOTAL_AMOUNT(state) {
    recalculateStoresTotalAmount(state);
  },
  ADD_SELECTED_PRODUCT(state, product) {
    const idx = getProductIndex(state.productByStoreShoppingCart.providerProducts, product);
    if (idx < 0) {
      state.productByStoreShoppingCart.providerProducts.push({
        ...product,
        quantity: 1,
      });
    } else {
      state.productByStoreShoppingCart.providerProducts[idx].quantity += 1;
    }
  },
  REMOVE_SELECTED_PRODUCT(state, product) {
    const idx = getProductIndex(state.productByStoreShoppingCart.providerProducts, product);
    state.productByStoreShoppingCart.providerProducts.splice(idx, 1);
  },
  SET_STORE_PRODUCTS(state, products) {
    state.storeProducts = products;
  },
};

export default mutations;
