import state from './state';
import mutations from './mutations';
import getters from './getters';
import actions from './actions';

const content = {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};

export default content;
