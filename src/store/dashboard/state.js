const state = () => ({
  isUpdatingShoppingList: false,
  shoppingList: [],
  shoppingCarts: [],
  shoppingProductsTypes: [],
  shoppingBasketId: '',
  productByStoreShoppingCart: {
    storeIds: [],
    providerProducts: [],
    providerName: '',
  },
  storeProducts: [],
  errorMsg: '',
});

export default state;
