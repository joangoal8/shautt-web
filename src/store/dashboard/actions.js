import { functions } from '@/plugins/firebase';
import firebase from 'firebase';
import calculateShoppingCartsRequest from '@/requests/calculate_shopping_carts_request';
import getProductsByStoreRequest from '@/requests/get_products_by_store_request';

const getShoppingBasketId = async (commit, state, user) => {
  let shoppingBasketId;
  if (state.shoppingBasketId) {
    shoppingBasketId = state.shoppingBasketId;
  } else {
    const shoppingBasketCollection = await firebase.firestore().collection('users').doc(user.uid).get();
    shoppingBasketId = await shoppingBasketCollection.get('shoppingBasketId');
    commit('SET_SHOPPING_BASKET_ID', shoppingBasketId);
  }
  return shoppingBasketId;
};

const updateShoppingListItemRequest = (shoppingBasketId, item) => {
  const lastUpdate = new Date().toLocaleString();
  let operation = 'EDIT';
  const updatedItem = { ...item };
  const { quantity } = item;
  if (quantity < 1) {
    operation = 'REMOVE';
  }
  return {
    id: shoppingBasketId,
    itemInfo: {
      itemId: item.itemId,
      quantity: item.quantity,
    },
    operationInfo: {
      operation,
      newItemInfo: {
        itemId: updatedItem.itemId,
        quantity,
        lastUpdate,
      },
    },
  };
};

const updateShoppingListRequest = async (commit, state, user, shoppingList) => {
  const shoppingBasketId = await getShoppingBasketId(commit, state, user);
  const request = { shoppingList: [], id: shoppingBasketId };
  shoppingList.forEach((item) => request.shoppingList.push(
    updateShoppingListItemRequest(shoppingBasketId, item),
  ));
  return request;
};

const actions = {
  async fetchShoppingProducts({ commit }) {
    firebase.auth().onAuthStateChanged(async () => {
      const getAllProductTypes = functions.httpsCallable('getProductTypes');
      getAllProductTypes().then((res) => {
        commit('SET_SHOPPING_PRODUCT_TYPES', res.data.results);
      }).catch((error) => {
        console.log({ error }); // TODO deal with error
      });
    });
  },

  async getShoppingList({ commit, state }) {
    await firebase.auth().onAuthStateChanged(async (user) => {
      commit('SET_SHOPPING_LIST_UPDATE_STATUS', true);
      const shoppingBasketId = await getShoppingBasketId(commit, state, user);
      const getShoppingList = functions.httpsCallable('getShoppingBasket');
      getShoppingList({ id: shoppingBasketId }).then((res) => {
        commit('SET_SHOPPING_LIST', res.data);
        commit('SET_SHOPPING_LIST_UPDATE_STATUS', false);
      }).catch((error) => {
        console.log(error); // TODO deal with error
      });
    });
  },

  async addItemToShoppingList({ commit, state }, item) {
    firebase.auth().onAuthStateChanged(async (user) => {
      const shoppingBasketId = await getShoppingBasketId(commit, state, user);
      const updateItemSB = functions.httpsCallable('updateItemSB');
      const updateShoppingBasketReq = await updateShoppingListItemRequest(shoppingBasketId, item);
      updateItemSB(updateShoppingBasketReq).then((res) => {
        commit('SET_SHOPPING_LIST', res.data);
      }).catch((err) => {
        console.log(err); // TODO deal with error
      });
    });
  },

  async updateShoppingList({ commit, state }, updatedShoppingList) {
    if (updatedShoppingList) {
      state.shoppingList = updatedShoppingList;
      commit('SET_SHOPPING_LIST_UPDATE_STATUS', true);
    }
    await firebase.auth().onAuthStateChanged(async (user) => {
      const updateShoppingList = functions.httpsCallable('updateShoppingList');
      const updateShoppingBasketReq = await updateShoppingListRequest(commit, state,
        user, state.shoppingList);
      updateShoppingList(updateShoppingBasketReq).then((res) => {
        commit('SET_SHOPPING_LIST', res.data);
        commit('SET_SHOPPING_LIST_UPDATE_STATUS', false);
      }).catch((err) => {
        console.log(err); // TODO deal with error
      });
    });
  },

  async getPricesFromStoresBasedOnPostalCode({ commit, state }, postalCode) {
    firebase.auth().onAuthStateChanged(async () => {
      if (!state.shoppingList) {
        await this.getShoppingList({ commit, state });
      }
      const calculateShoppingCarts = functions.httpsCallable('calculateShoppingCarts');
      calculateShoppingCarts(calculateShoppingCartsRequest(postalCode, state.shoppingList))
        .then((res) => {
          commit('SET_SHOPPING_CARTS', res.data.results);
        }).catch((e) => {
          console.log(e); // TODO deal with error
        });
    });
  },

  async getProductsFromStore({ commit }, storeId) {
    firebase.auth().onAuthStateChanged(async () => {
      const getProductsByStore = functions.httpsCallable('getProductsByStore');
      getProductsByStore(getProductsByStoreRequest(storeId))
        .then((res) => {
          commit('SET_STORE_PRODUCTS', res.data);
        }).catch((e) => {
          console.log(e); // TODO deal with error
        });
    });
  },
};

export default actions;
