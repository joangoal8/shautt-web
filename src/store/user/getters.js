const getters = {
  user: (state) => state.user,
  loginErrorMsg: (state) => state.errorMsg,
  isUserLogged: (state) => state.user.loggedIn,
  postalCode: (state) => sessionStorage.getItem('postalCode') || state.postalCode,
};

export default getters;
