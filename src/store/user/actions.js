import firebase from 'firebase';
import router from '@/router';

const actions = {
  fetchUser({ commit }) {
    firebase.auth().onAuthStateChanged((user) => {
      commit('SET_LOGGED_IN', user !== null);
      if (user) {
        commit('SET_USER', {
          displayName: user.displayName,
          email: user.email,
        });
        if (router.currentRoute.name === 'Login') {
          router.push({ name: 'Dashboard/ShoppingList' });
        }
      } else {
        commit('SET_USER', null);
      }
    });
  },
  // eslint-disable-next-line no-unused-vars
  async logIn({ commit }, { provider, email, password }) {
    let loginResponse;
    try {
      if (provider === 'shautt') {
        loginResponse = await firebase.auth()
          .signInWithEmailAndPassword(email, password);
      } else if (provider === 'google') {
        const gProvider = new firebase.auth.GoogleAuthProvider();
        loginResponse = await firebase.auth()
          .signInWithPopup(gProvider);
      }
    } catch (e) {
      loginResponse = e;
    }
    if (loginResponse.code) {
      commit('SET_LOGIN_ERROR_MSG', loginResponse.message);
    } else {
      commit('SET_USER', {
        displayName: loginResponse.user.displayName,
        email: loginResponse.user.email,
      });
    }
  },
  async logOut({ commit }) {
    await firebase.auth().signOut();
    commit('SET_USER', {});
  },

  setPostalCode({ commit }, postalCode) {
    commit('SET_POSTAL_CODE', postalCode);
  },
};

export default actions;
