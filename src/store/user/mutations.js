const mutations = {
  SET_LOGGED_IN(state, value) {
    state.user.loggedIn = value;
  },
  SET_USER(state, data) {
    state.user.data = data;
  },
  SET_LOGIN_ERROR_MSG(state, msg) {
    state.errorMsg = msg;
  },
  RESET_LOGIN_ERROR_MSG(state) {
    state.errorMsg = '';
  },
  RESET_USER_DATA(state) {
    state.user = {};
    state.errorMsg = '';
  },
  SET_POSTAL_CODE(state, postalCode) {
    sessionStorage.setItem('postalCode', postalCode);
    state.postalCode = postalCode;
  },
};

export default mutations;
