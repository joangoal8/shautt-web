const state = () => ({
  user: {
    loggedIn: false,
    data: null,
  },
  postalCode: '',
  errorMsg: '',
});

export default state;
