import Vuex from 'vuex';
import Vue from 'vue';
import user from './user/index';
import dashboard from './dashboard/index';

Vue.use(Vuex);
const store = new Vuex.Store({
  modules: {
    user,
    dashboard,
  },
  state: () => ({}),
  getters: {},
  mutations: {},
});

export default store;
