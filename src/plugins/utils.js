import Vue from 'vue';
import * as assets from '@/utils/assets';
import * as analytic from '@/utils/analytic-tools';

export default () => Object.defineProperty(Vue.prototype, '$utils',
  { value: { assets, analytic } });
