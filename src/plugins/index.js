import 'bootstrap/dist/css/bootstrap.css';
import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import BootstrapVue from 'bootstrap-vue';
import { apiInstall } from '@/plugins/Api';
import VueCookies from 'vue-cookies';
import { firebaseInstall, getMeasurementId } from '@/plugins/firebase';
import VueGtag from 'vue-gtag';
import router from '@/router';

const useVuePlugins = () => {
  Vue.use(Vuex);
  Vue.use(VueRouter);
  Vue.use(BootstrapVue);
  Vue.use(apiInstall);
  Vue.use(VueCookies);
  Vue.use(firebaseInstall);
  Vue.use(VueGtag, {
    config: { id: getMeasurementId() },
  }, router);
};

export default useVuePlugins;
