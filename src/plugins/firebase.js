import Vue from 'vue';
import firebase from 'firebase';

require('firebase/firestore');

const firebaseConfig = {
  apiKey: process.env.VUE_APP_FIREBASE_API_KEY,
  authDomain: process.env.VUE_APP_FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.VUE_APP_FIREBASE_DATABASE_URL,
  projectId: process.env.VUE_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.VUE_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.VUE_APP_FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.VUE_APP_FIREBASE_APP_ID,
  measurementId: process.env.VUE_APP_FIREBASE_MEASUREMENT_ID,
};

firebase.initializeApp(firebaseConfig);
firebase.app().functions('europe-west6');
const functions = firebase.app().functions('europe-west6');

const firebaseInstall = () => {
  Vue.prototype.$firebase = firebase;
  Vue.prototype.$functions = functions;
  if (process.env.VUE_APP_FUNCTIONS_ENV === 'local') {
    firebase.functions().useEmulator('localhost', 5001);
    functions.useEmulator('localhost', 5001);
  }
};

const getMeasurementId = () => firebaseConfig.measurementId;

firebase.getCurrentUser = () => new Promise((resolve, reject) => {
  const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
    unsubscribe();
    resolve(user);
  }, reject);
});

export {
  getMeasurementId,
  firebaseInstall,
  functions,
  firebase,
};
