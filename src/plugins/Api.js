import axios from 'axios';
import Vue from 'vue';

function createInstance() {
  return axios.create({
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      // 'Authorization': `Bearer ${localStorage.token}`
    },
    // baseURL: 'https://shaut-tech.ew.r.appspot.com/shautt-engine/v1',
  });
}

const axiosInstance = createInstance();

const apiInstall = () => {
  Vue.prototype.$api = axiosInstance;
};

export {
  axiosInstance,
  apiInstall,
};
