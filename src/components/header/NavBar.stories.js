import Vue from 'vue';
import 'bootstrap/dist/css/bootstrap.css';
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
import utilsPlugin from '../../../__mocks__/utils';
import NavBar from './NavBar.vue';
import headerContentJson from '../../store/content/texts/header.json';
import store from '../../store/index';
import router from '../../router';

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.use(utilsPlugin);

export default {
  title: 'Header/NavBar',
  component: NavBar,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { NavBar },
  template: '<nav-bar :content="headerContent"/>',
  store,
  router,
});

export const defaultHeader = Template.bind({});
defaultHeader.args = {
  headerContent: headerContentJson.text,
};
