import FindStoresByZipCode from '@/components/v2/find-stores/';
import { i18n } from '@/stories/genericTemplate';

export default {
  title: 'V2/SmallComponents/FindStoresByZipCode',
  component: FindStoresByZipCode,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { FindStoresByZipCode },
  i18n,
  template: '<find-stores-by-zip-code  :postal-code="postalCode" :button-text="buttonText" :input-placeholder="inputPlaceholder"/>',
});

export const defaultTemplate = Template.bind({});
defaultTemplate.args = {
  postalCode: 28008,
  buttonText: 'Search stores',
  inputPlaceholder: 'Insert ZipCode',
};
