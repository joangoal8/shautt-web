import { i18n } from '@/stories/genericTemplate';
import ItemsTable from '@/components/v2/shopping-list/ItemsTable.vue';
import { getShoppingProducts } from '@/components/v2/search-product-type/__mocks__';
import { getShoppingList } from '@/components/v2/__mocks__';

export default {
  title: 'V2/UserArea/ShoppingList/ItemsTable',
  component: ItemsTable,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { ItemsTable },
  i18n,
  template: '<items-table :shopping-list="shoppingList" :products="products" :is-updating="isUpdating"/>',
});

export const withProducts = Template.bind({});
withProducts.args = {
  shoppingList: getShoppingList(),
  products: getShoppingProducts(),
  isUpdating: false,
};

export const loading = Template.bind({});
loading.args = {
  shoppingList: [],
  products: [],
  isUpdating: true,
};
