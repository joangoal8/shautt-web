import { i18n } from '@/stories/genericTemplate';
import ShoppingList from '@/components/v2/shopping-list/';
import { getShoppingProducts } from '@/components/v2/search-product-type/__mocks__';
import { getShoppingList } from '@/components/v2/__mocks__';

export default {
  title: 'V2/UserArea/ShoppingList',
  component: ShoppingList,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { ShoppingList },
  i18n,
  template: '<shopping-list :shopping-list="shoppingList" :products="products"/>',
});

export const withProducts = Template.bind({});
withProducts.args = {
  shoppingList: getShoppingList(),
  products: getShoppingProducts(),
};

export const loading = Template.bind({});
loading.args = {
  shoppingList: [],
  products: [],
};
