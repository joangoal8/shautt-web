import { i18n } from '@/stories/genericTemplate';
import SelectStores from '@/components/v2/shopping-carts/SelectStores.vue';
import { getShoppingCarts } from '@/components/v2/__mocks__';

export default {
  title: 'V2/UserArea/CompareMarkets/SelectStores',
  component: SelectStores,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { SelectStores },
  i18n,
  template: '<select-stores :shopping-carts="shoppingCarts" :is-loading-status="isLoading" :display="isDisplay"/>',
});

export const defaultView = Template.bind({});
defaultView.args = {
  shoppingCarts: getShoppingCarts(),
  isLoading: false,
  isDisplay: true,
};
