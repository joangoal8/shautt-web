import { i18n } from '@/stories/genericTemplate';
import SelectedProducts from '@/components/v2/shopping-carts/SelectedProducts.vue';
import { getShoppingCarts } from '@/components/v2/__mocks__';

export default {
  title: 'V2/UserArea/CompareMarkets/SelectedProducts',
  component: SelectedProducts,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { SelectedProducts },
  i18n,
  template: '<selected-products :products="getShoppingCarts()[storeIndex].shoppingCartProducts"/>',
});

export const defaultView = Template.bind({});
defaultView.args = {
  storeIndex: 0,
  getShoppingCarts,
  isLoading: false,
  isDisplay: true,
};
