import { i18n } from '@/stories/genericTemplate';
import RegisterForm from './index';

export default {
  title: 'V2/Login/RegisterForm',
  component: RegisterForm,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { RegisterForm },
  i18n,
  template: '<register-form :error-message="errorMessage"/>',
});

export const defaultView = Template.bind({});
defaultView.args = {
  errorMessage: '',
};
