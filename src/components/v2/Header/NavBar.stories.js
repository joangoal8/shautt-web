import { i18n, getStore } from '@/stories/genericTemplate';
import NavBar from './index';

export default {
  title: 'V2/Header/NavBar',
  component: NavBar,
};

const user = {
  state: {
    user: {
      loggedIn: true,
    },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { NavBar },
  i18n,
  store: getStore({}),
  template: '<nav-bar/>',
});

export const defaultHeader = Template.bind({});
defaultHeader.args = {
};

const LoggedTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { NavBar },
  i18n,
  store: getStore({ user }),
  template: '<nav-bar/>',
});

export const loggedHeader = LoggedTemplate.bind({});
loggedHeader.args = {
};
