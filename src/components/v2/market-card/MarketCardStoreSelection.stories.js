import { i18n } from '@/stories/genericTemplate';
import StoreSelection from '@/components/v2/market-card/StoreSelection.vue';
import { getShoppingCarts } from '@/components/v2/__mocks__';

export default {
  title: 'V2/UserArea/CompareMarkets/MarketCards',
  component: StoreSelection,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { StoreSelection },
  i18n,
  template: '<store-selection :cart="cart" :is-selected="isSelected"/>',
});

export const storeSelectionView = Template.bind({});
storeSelectionView.args = {
  cart: getShoppingCarts()[0],
  isSelected: true,
};
