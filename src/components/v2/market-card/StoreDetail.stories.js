import { i18n } from '@/stories/genericTemplate';
import StoreDetails from '@/components/v2/market-card/StoreDetails.vue';
import { getShoppingCarts } from '@/components/v2/__mocks__';

export default {
  title: 'V2/UserArea/CompareMarkets/MarketCards',
  component: StoreDetails,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { StoreDetails },
  i18n,
  template: '<store-details :cart="cart"/>',
});

export const storeDetailsView = Template.bind({});
storeDetailsView.args = {
  cart: getShoppingCarts()[0],
  isSelected: true,
};
