import { i18n } from '@/stories/genericTemplate';
import ProductItem from '@/components/v2/market-card/ProductItem.vue';
import { getShoppingCartProducts } from '@/components/v2/__mocks__';

export default {
  title: 'V2/UserArea/CompareMarkets/MarketCards',
  component: ProductItem,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { ProductItem },
  i18n,
  template: '<product-item :product-item="getShoppingCartProducts(storeIndex, itemIndex).selectedProduct" :selected="isSelected" :is-selectable="isSelectable" :quantity="getShoppingCartProducts(storeIndex, itemIndex).quantity"/>',
});

export const productItemView = Template.bind({});
productItemView.args = {
  storeIndex: 0,
  itemIndex: 0,
  getShoppingCartProducts,
  isSelected: true,
  isSelectable: false,
};
