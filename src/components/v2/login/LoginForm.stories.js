import { i18n } from '@/stories/genericTemplate';
import LoginForm from './index';

export default {
  title: 'V2/Login/LoginForm',
  component: LoginForm,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { LoginForm },
  i18n,
  template: '<login-form :error-message="errorMessage" />',
});

export const defaultView = Template.bind({});
defaultView.args = {
  errorMessage: '',
};
