import ShauttLogo from '@/components/v2/logos/index';
import { i18n } from '@/stories/genericTemplate';

export default {
  title: 'V2/SmallComponents/Logo',
  component: ShauttLogo,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { ShauttLogo },
  i18n,
  template:
    '<shautt-logo :hideBrandTitle="hideBrandTitle" />',
});

export const ShauttLogoStory = Template.bind({});
ShauttLogoStory.args = {
  hideBrandTitle: false,
};
