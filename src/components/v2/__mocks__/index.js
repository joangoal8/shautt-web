import shoppingList from './shoppingBasket.json';
import shoppingCarts from './calculateShoppingCartsResponse.json';

const getShoppingList = () => shoppingList.result;

// eslint-disable-next-line max-len
const getShoppingCarts = () => shoppingCarts.result.results.filter((cart) => cart.stores.length === 1);

// eslint-disable-next-line max-len
const getShoppingCartProducts = (storeIndex, item) => shoppingCarts.result.results[storeIndex].shoppingCartProducts[item];

export {
  getShoppingList,
  getShoppingCarts,
  getShoppingCartProducts,
};
