import { i18n } from '@/stories/genericTemplate';
import ChangeProductModal from '@/components/v2/change-product-modal/ChangeProductModal.vue';
import { getShoppingCarts } from '@/components/v2/__mocks__';

export default {
  title: 'V2/UserArea/CompareMarkets/ChangeProductModal',
  component: ChangeProductModal,
};

// eslint-disable-next-line max-len
const getProducts = (storeIndex, productIndex) => getShoppingCarts()[storeIndex].shoppingCartProducts[productIndex].otherOptionsProducts;
// eslint-disable-next-line max-len
const getProductType = (storeIndex, productIndex) => getShoppingCarts()[storeIndex].shoppingCartProducts[productIndex].productType.productTypeName;
// eslint-disable-next-line max-len
const getSelectedProductId = (storeIndex, productIndex) => getShoppingCarts()[storeIndex].shoppingCartProducts[productIndex].selectedProduct.id;
// eslint-disable-next-line no-return-assign,max-len
const onChangeSelectedProduct = (selectedItem, storeIndex, productIndex) => getShoppingCarts()[storeIndex].shoppingCartProducts[productIndex].selectedProduct = selectedItem.item;

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { ChangeProductModal },
  i18n,
  template: '<div><b-button @click="$refs.changeProductModal.open = true">Open Modal</b-button><change-product-modal ref="changeProductModal" :products="getProducts(storeIndex, productIndex)" :product-type="getProductType(storeIndex, productIndex)" :selected-product-id="getSelectedProductId(storeIndex, productIndex)" @changeSelectedProduct="onChangeSelectedProduct($event, storeIndex, productIndex)"/></div>',
});

export const defaultView = Template.bind({});
defaultView.args = {
  storeIndex: 0,
  productIndex: 0,
  getShoppingCarts,
  getProducts,
  getProductType,
  getSelectedProductId,
  onChangeSelectedProduct,
};
