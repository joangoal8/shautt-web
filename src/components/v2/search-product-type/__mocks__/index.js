import productTypes from './productTypes.json';

const getShoppingProducts = () => {
  const shoppingProducts = [];
  productTypes.data.map((category) => category.items.map((item) => shoppingProducts.push({
    ...item,
    categoryName: category.name,
  })));
  return shoppingProducts;
};

const dummy = () => {};

export {
  getShoppingProducts,
  dummy,
};
