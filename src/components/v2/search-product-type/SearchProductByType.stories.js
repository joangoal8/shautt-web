import { i18n, getStore } from '@/stories/genericTemplate';
import SearchProductByType from '@/components/v2/search-product-type/index';
import { getShoppingProducts } from '@/components/v2/search-product-type/__mocks__';

export default {
  title: 'V2/UserArea/ShoppingList/SearchProductByType',
  component: SearchProductByType,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { SearchProductByType },
  i18n,
  store: getStore({}),
  template: '<search-product-by-type :products="shoppingProducts"/>',
});

export const withProducts = Template.bind({});
withProducts.args = {
  shoppingProducts: getShoppingProducts(),
};

export const loading = Template.bind({});
loading.args = {
  shoppingProducts: [],
};
