import { i18n } from '@/stories/genericTemplate';
import CookieConsent from './CookieConsent.vue';

export default {
  title: 'V2/CookiePolicy',
  component: CookieConsent,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { CookieConsent },
  i18n,
  template: '<div><b-button @click="$refs.cookieConsent.modalShow=true">Display cookie consent</b-button><cookie-consent ref="cookieConsent"/></div>',
});

export const defaultView = Template.bind({});
defaultView.args = {
};
