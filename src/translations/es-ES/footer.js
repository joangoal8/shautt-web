const footer = {
  info: {
    title: '{BRAND}',
    description: 'Enfocados a la mejora y automatización del proceso de compra. Para cualquier duda, sugerencia o pregunta podéis contactar en {CONTACT}',
    contact: 'contact@shautt.com',
  },
  socialMediaLinks: [
    {
      icon: 'instagram',
      link: 'https://www.instagram.com/shautt.app',
    },
    {
      icon: 'linkedin',
      link: 'https://www.linkedin.com/company/shautt',
    },
    {
      icon: 'youtube',
      link: 'https://www.youtube.com/channel/UCN_9OqUkNPJgFYc0VEdEvVA',
    },
  ],
  links: {
    title: 'LINKS',
    links: [
      {
        name: 'Home',
        link: '/',
      },
      {
        name: 'Términos y condiciones',
        link: '/terms-conditions',
      },
      {
        name: 'Política de privacidad',
        link: '/privacy-policy',
      },
    ],
  },
  developer: {
    text: 'Developed by {BRAND} &copy; 2020. All right reserved.',
    author: 'Shautt',
    link: 'https://www.linkedin.com/company/shautt',
  },
};

export default footer;
