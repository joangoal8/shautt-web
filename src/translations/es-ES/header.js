const header = {
  signOut: {
    text: 'Cerrar sesión',
  },
  items: {
    logged: {
      center: {
        name: 'Dashboard',
        links: [{
          link: '/dashboard/shopping-list',
          name: 'Mi Lista de la compra',
        }],
      },
    },
    notLogged: {
      center: {

      },
      right: {
        loginBtn: {
          text: 'ENTRAR',
          link: '/login',
        },
        signUpBtn: {
          text: 'REGISTRAR',
          link: '/register',
        },
      },
    },
  },
};

export default header;
