const cookiePolicy = {
  introLayer: {
    title: '',
    description: 'We use optional third-party analytics cookies to understand how you use Shautt.com so we can build better products.\nYou can always update your selection by clicking "Learn More" at the bottom of the page. For more information, see our {privacyPolicy}.',
    descriptionArgument1: 'Privacy and Policy',
    learnMoreBtn: 'Learn More',
    agreeBtn: 'Agree & Close',
  },
  learnMoreLayer: {
    typeOfCookies: [
      {
        title: 'Essential cookies',
        id: 'essential',
        description: 'Essential cookies are used to perform essential website functions, for example, to keep you logged in the page.',
        message: 'Always Active',
        optional: false,
      },
      {
        title: 'Analytics cookies',
        id: 'analytical',
        description: 'Analytics cookies are used to understand how you use our websites so we can make them better, for example, to gather information about how many clicks you need to accomplish a task.',
        acceptBtn: 'Accept',
        rejectBtn: 'Reject',
        optional: true,
      },
    ],
    agreeBtn: 'Save preferences',
  },
};

export default cookiePolicy;
