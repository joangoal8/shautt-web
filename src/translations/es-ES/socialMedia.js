const socialMedia = [
  {
    icon: 'youtube',
    link: 'https://www.youtube.com/channel/UCN_9OqUkNPJgFYc0VEdEvVA',
  },
  {
    icon: 'linkedin',
    link: 'https://www.linkedin.com/company/shautt',
  },
  {
    icon: 'instagram',
    link: 'https://www.instagram.com/shautt.app',
  },
];

export default socialMedia;
