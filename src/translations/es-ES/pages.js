const pages = {
  termsConditions: {
    text: 'Términos y condiciones',
    link: '/terms-conditions',
  },
  privacyPolicy: {
    text: 'Política de privacidad',
    link: '/privacy-policy',
  },
  shoppingList: {
    text: 'Mi lista de la compra',
    link: '/dashboard/shopping-list',
  },
  home: {
    text: 'Home',
    link: '/',
  },
};

export default pages;
