const register = {
  form: {
    email: {
      label: 'EMAIL',
      placeholder: 'shautt@shautt.com',
      description: '',
    },
    name: {
      label: 'NOMBRE',
      placeholder: 'Don Mario',
      description: '',
    },
    password: {
      label: 'PASSWORD',
      placeholder: 'Contraseña',
      description: '',
    },
    buttons: {
      submit: 'REGISTRAR',
    },
  },
  registerQuestionMessage: '¿Ya tienes una cuenta en Shautt?',
  signInText: '¡Hazte login!',
};

export default register;
