const login = {
  form: {
    email: {
      label: 'EMAIL',
      placeholder: 'shautt@shautt.com',
      description: '',
    },
    password: {
      label: 'PASSWORD',
      placeholder: 'Contraseña',
      description: '',
    },
    buttons: {
      submit: 'ENTRAR',
      google: 'Entrar con Gmail',
    },
  },
  signUpQuestionMessage: '¿No tienes una cuenta en Shautt todavía?',
  signUpText: '¡Regístrate!',
  errors: {
    loginExistingAccount: 'Email o password incorrectos.',
    loginNonExistingAccount: 'Email no registrado en Shautt. ',
  },
};

export default login;
