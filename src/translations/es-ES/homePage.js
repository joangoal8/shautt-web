const homePage = {
  intro: {
    headerTitle: 'Optimiza tu compra comparando y comprando en diferentes establecimientos',
    buttons: [
      {
        id: 'appleStoreBtn',
        description: '',
        iconSrc: 'downloadAppStoreAppleES.svg',
        alt: 'Click to redirect to apple app store',
        customClass: 'cs-home-intro-app-store-icon',
        link: 'https://apps.apple.com/es/app/shautt/id1540642536?l=es',
        type: 'image',
      },
      {
        id: 'videoBtn',
        description: 'Ver video',
        customClass: 'cs-home-intro-button-video',
        type: 'button',
      },
    ],
    backgroundImage: 'shauttHomePageImage.webp',
    modalVideoName: 'demoShuattAppShopping.mp4',
  },
  firstSection: {
    title: 'Proceso de compra automatizada',
    benefits: [
      {
        title: 'Genera tu lista de la compra',
        description: 'Añade todos tus productos consumibles a los cuales estás habituado a comprar en tus supermercados favoritos a tu lista de la compra.',
      },
      {
        title: 'Compara y elige la mejor opción',
        description: 'La lista de la compra es comparada con diferentes establecimientos para obtener las mejores opciones.',
      },
      {
        title: 'Modifica tu carrito a tu gusto',
        description: 'Puedes editar tu carrito de la compra eligiendo las mejores opciones de productos entre un abanico extenso de posibilidades.',
      },
    ],
    carouselImages: ['shoppingList_cropped.webp', 'result_cropped.webp', 'shoppingCart_cropped.webp'],
  },
  secondSection: {
    title: 'Compara tus productos para saber el mejor precio/calidad',
    benefits: [
      {
        title: 'Escanea el código de barras de tu producto',
        description: 'Con el código de barras se obtiene el tipo de producto el cual se va hacer la comparativa.',
      },
      {
        title: 'Mira los mejores 30 tipos de productos',
        description: 'El algoritmo los compara y te da los mejores 30 resultados referente a ese tipo ordenados en precio/calidad de los diferentes establecimientos.',
      },
      {
        title: 'Añade a tu lista el tipo de producto',
        description: 'Añade el producto a la lista para posteriormente obtener tu carrito de la compra con el mejor criterio calidad/precio.',
      },
    ],
    carouselImages: ['scannerView_cropped.webp', 'comparatorView_cropped.webp'],
    videoButton: {
      description: 'Ver video',
    },
    modalVideoName: 'scannerDemoShautt.mp4',
  },
};

export default homePage;
