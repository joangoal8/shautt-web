import socialMedia from '@/translations/es-ES/socialMedia';
import login from '@/translations/es-ES/loginPage';
import register from '@/translations/es-ES/registerPage';
import pages from '@/translations/es-ES/pages';
import userArea from '@/translations/es-ES/userArea';
import cookiePolicy from '@/translations/es-ES/cookiePolicy';
import header from '@/translations/es-ES/header';
import homePage from '@/translations/es-ES/homePage';
import privacyPolicyPage from '@/translations/es-ES/privacyPolicyPage';
import termsConditionsPage from '@/translations/es-ES/termsConditionsPage';
import footer from '@/translations/es-ES/footer';

const translations = {
  brand: {
    name: 'Shautt',
  },
  socialMedia,
  login,
  register,
  pages,
  userArea,
  cookiePolicy,
  header,
  homePage,
  privacyPolicyPage,
  termsConditionsPage,
  footer,
};

export default translations;
