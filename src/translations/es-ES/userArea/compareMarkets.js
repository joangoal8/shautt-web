const compareMarkets = {
  title: 'Resultados',
  findStore: {
    zipCodeMessage: 'Para el código postal',
    btnMessage: 'Buscar tiendas',
    filterBtn: 'Filtro',
  },
  selectStores: {
    title: 'Supermecados disponibles en tu código postal',
    description: '¡Elige los mercados para comparar precios!',
    okBtn: 'Comparar precios',
    loading: 'Cargando...',
  },
  storeDetails: {
    totalPrice: 'Total:',
  },
  productItem: {
    changeProductMessage: 'Cambiar producto',
    btn: {
      productChanged: '¡Listo!',
    },
  },
};

export default compareMarkets;
