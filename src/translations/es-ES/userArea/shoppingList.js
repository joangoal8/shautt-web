const shoppingList = {
  title: 'Mi lista de la compra',
  searchProductByCategory: {
    finderPlaceHolder: '¿Qué necesitas comprar?',
  },
  btn: {
    updateList: 'Refrescar',
    saveList: 'Guardar',
  },
  postalCode: {
    btn: 'Siguiente >>',
    placeholder: 'Inserir código postal',
  },
  table: {
    item: {
      product: 'Productos',
      quantity: 'Cantidad',
    },
  },
};

export default shoppingList;
