import shoppingList from '@/translations/es-ES/userArea/shoppingList';
import compareMarkets from '@/translations/es-ES/userArea/compareMarkets';

const userArea = {
  shoppingList,
  compareMarkets,
};

export default userArea;
