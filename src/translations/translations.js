import { login } from './loginPage';
import { register } from './registerPage';

const socialMedia = [
  {
    icon: 'youtube',
    link: 'https://www.youtube.com/channel/UCN_9OqUkNPJgFYc0VEdEvVA',
  },
  {
    icon: 'linkedin',
    link: 'https://www.linkedin.com/company/shautt',
  },
  {
    icon: 'instagram',
    link: 'https://www.instagram.com/shautt.app',
  },
];

const pages = {
  termsConditions: {
    text: 'Términos y condiciones',
    link: '/terms-conditions',
  },
  privacyPolicy: {
    text: 'Política de privacidad',
    link: '/privacy-policy',
  },
  shoppingList: {
    text: 'Mi lista de la compra',
    link: '/dashboard/shopping-list',
  },
  home: {
    text: 'Home',
    link: '/',
  },
};

const userArea = {
  shoppingList: {
    title: 'Mi lista de la compra',
    searchProductByCategory: {
      finderPlaceHolder: '¿Qué necesitas comprar?',
    },
    btn: {
      updateList: 'Refrescar',
      saveList: 'Guardar',
    },
    postalCode: {
      btn: 'Siguiente >>',
      placeholder: 'Inserir código postal',
    },
    table: {
      item: {
        product: 'Productos',
        quantity: 'Cantidad',
      },
    },
  },
  compareMarkets: {
    title: 'Resultados',
    findStore: {
      zipCodeMessage: 'Para el código postal',
      btnMessage: 'Buscar tiendas',
      filterBtn: 'Filtro',
    },
    selectStores: {
      title: 'Supermecados disponibles en tu código postal',
      description: '¡Elige los mercados para comparar precios!',
      okBtn: 'Comparar precios',
      loading: 'Cargando...',
    },
    storeDetails: {
      totalPrice: 'Total:',
    },
    productItem: {
      changeProductMessage: 'Cambiar producto',
      btn: {
        productChanged: '¡Listo!',
      },
    },
  },
};

const translations = {
  'es-ES': {
    brand: {
      name: 'Shautt',
    },
    socialMedia,
    login,
    register,
    pages,
    userArea,
    header: {
      signOut: {
        text: 'Cerrar sesión',
      },
      items: {
        logged: {
          center: {
            name: 'Dashboard',
            links: [{
              link: '/dashboard/shopping-list',
              name: 'Mi Lista de la compra',
            }],
          },
        },
        notLogged: {
          center: {

          },
          right: {
            loginBtn: {
              text: 'ENTRAR',
              link: '/login',
            },
            signUpBtn: {
              text: 'REGISTRAR',
              link: '/register',
            },
          },
        },
      },
    },
  },
};

export default {
  ...translations,
};
