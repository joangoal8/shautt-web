import esES from '@/translations/es-ES';

const translations = {
  'es-ES': esES,
};

export default translations;
