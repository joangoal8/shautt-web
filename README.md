shautt-web

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Serve production app
```
serve -s dist
```

### Lints and fixes files
```
npm run lint
```

### TODO
- Header is not updating when user completes login
- Dashboard page is empty when redirected

