const WHITE_SPACES_REGEX = / /g;
const ESCAPE_SPACES_REGEX = /\n/g;
const EMPTY_STRING = '';

function removeSpacesBetweenReplacedKeys(text) {
  return text.replace(WHITE_SPACES_REGEX, EMPTY_STRING).replace(ESCAPE_SPACES_REGEX, EMPTY_STRING);
}

export {
  removeSpacesBetweenReplacedKeys,
  EMPTY_STRING,
};
