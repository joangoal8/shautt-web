import Vuex from 'vuex';
import getters from '@/store/content/getters';

const actions = {
  loadContentForView: jest.fn(),
};

const state = {
  content: {
    cookiePolicy: {
      introLayer: {
        description: 'INTRO_LAYER_DESC_%1$s',
        descriptionArgument1: 'DESC_ARG_1',
        learnMoreBtn: 'INTRO_BTN_LM',
        agreeBtn: 'INTRO_BTN_OK',
      },
      learnMoreLayer: {
        typeOfCookies: [
          {
            title: 'LEARNMORE_LAYER_ESSENTIAL_COOKIES',
            id: 'essential',
            description: 'LEARNMORE_LAYER_ESSENTIAL_DESC',
            message: 'LEARNMORE_LAYER_ESSENTIAL_ALWAYS_ACTIVE',
            optional: false,
          },
          {
            title: 'LEARNMORE_LAYER_ANALYTICS_COOKIES',
            id: 'analytical',
            description: 'LEARNMORE_LAYER_ANALYTICS_DESC',
            acceptBtn: 'LEARNMORE_LAYER_BTN_ACCEPT',
            rejectBtn: 'LEARNMORE_LAYER_BTN_REJECT',
            optional: true,
          },
        ],
        agreeBtn: 'BTN_SAVEPREFERENCES',
      },
    },
  },
};

function getContentStore() {
  return new Vuex.Store({
    modules: {
      content: {
        actions,
        getters,
        state,
        namespaced: true,
      },
    },
  });
}

export {
  getContentStore,
  actions,
  getters,
  state,
};
