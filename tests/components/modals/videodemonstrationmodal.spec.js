import { createLocalVue, mount } from '@vue/test-utils';
import BootstrapVue from 'bootstrap-vue';
import VideoDemonstrationModal from '@/components/modals/VideoDemonstrationModal.vue';
import utilsPlugin from '@/plugins/utils';

const localVue = createLocalVue();
localVue.use(BootstrapVue);
localVue.use(utilsPlugin);
const VIDEO_NAME = 'the_video.mp4';

describe('Component: VideoDemonstrationModal.vue', () => {
  test('Expect to render the video', async () => {
    VideoDemonstrationModal.props.videoName(VIDEO_NAME);
    const wrapper = mount(VideoDemonstrationModal, {
      localVue,
      propsData: {
        videoName: VIDEO_NAME,
      },
    });
    expect(wrapper.vm.getVideoSrcPath()).toEqual(`/videos/${VIDEO_NAME}`);
  });
});
