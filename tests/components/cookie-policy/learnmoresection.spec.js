import { createLocalVue, shallowMount } from '@vue/test-utils';
import BootstrapVue, { BButton } from 'bootstrap-vue';
import LearnMoreSection from '@/components/v2/cookie-policy/LearnMoreSection.vue';
import { state } from '../../store/content/content.mock';

const localVue = createLocalVue();
localVue.use(BootstrapVue);

describe('Component: CookieConsent.vue', () => {
  it('Should render the section with the options', () => {
    const content = state.content.cookiePolicy.learnMoreLayer;
    const wrapper = shallowMount(LearnMoreSection, {
      localVue,
      propsData: {
        content,
        analytics: true,
      },
    });
    for (const ck of content.typeOfCookies) {
      expect(wrapper.text()).toContain(ck.title);
      expect(wrapper.text()).toContain(ck.description);
      if (ck.optional) {
        expect(wrapper.text()).toContain(ck.acceptBtn);
        expect(wrapper.text()).toContain(ck.rejectBtn);
      } else {
        expect(wrapper.text()).toContain(ck.message);
      }
    }
  });

  it('Should emit an update if analytical status is changed', () => {
    const content = state.content.cookiePolicy.learnMoreLayer;
    const wrapper = shallowMount(LearnMoreSection, {
      localVue,
      propsData: {
        content,
        analytics: true,
      },
    });
    wrapper.findAllComponents(BButton).at(1).trigger('click');
    expect(wrapper.emitted('updateAnalytical')[0]).toEqual([true]);
  });
});
