import { createLocalVue, shallowMount } from '@vue/test-utils';
import BootstrapVue, { BCard } from 'bootstrap-vue';
import CookieConsent from '@/components/v2/cookie-policy/CookieConsent.vue';
import Vuex from 'vuex';
import FooterBtn from '@/components/v2/cookie-policy/FooterBtn.vue';
import LearnMoreSection from '@/components/v2/cookie-policy/LearnMoreSection.vue';
import VueCookies from 'vue-cookies';
import utilsPlugin from '@/plugins/utils';
import { removeSpacesBetweenReplacedKeys } from '../../utils/utils';
import { getContentStore, actions, state } from '../../store/content/content.mock';
import AnalyticsStub from '../../mixins/analytics.stub';

const localVue = createLocalVue();
localVue.use(BootstrapVue);
localVue.use(Vuex);
localVue.use(VueCookies);
localVue.use(utilsPlugin);

describe('Component: CookieConsent.vue', () => {
  let store;
  beforeEach(() => {
    store = getContentStore();
  });
  it('Should render description with replacement', () => {
    // const wrapper = shallowMount(CookieConsent, {
    //   localVue,
    //   store,
    // });
    // TODO fix
    // const expectedStr = replaceContentPlaceholder(state.content.cookieP
    // olicy.introLayer.description,
    //   state.content.cookiePolicy.introLayer.descriptionArgument1);
    // const viewText = removeSpacesBetweenReplacedKeys(wrapper.text());
    // expect(actions.loadContentForView).toHaveBeenCalled();
    // expect(viewText).toContain(expectedStr);
    // const footerBtn = wrapper.findComponent(FooterBtn);
    // const learnMoreSection = wrapper.findComponent(LearnMoreSection);
    // expect(footerBtn.exists()).toBe(true);
    // expect(learnMoreSection.exists()).toBe(false);
  });

  it('Should exist learnMore when introLayer is false', () => {
    const wrapper = shallowMount(CookieConsent, {
      localVue,
      store,
      data() {
        return {
          introLayer: false,
        };
      },
    });
    const footerBtn = wrapper.findComponent(FooterBtn);
    const learnMoreSection = wrapper.findComponent(LearnMoreSection);
    expect(footerBtn.exists()).toBe(true);
    expect(learnMoreSection.exists()).toBe(true);
  });

  it('Should create a cookie with the content default', () => {
    const consentCookieExpected = {
      essentialsCookies: true,
      analyticsCookies: true,
    };
    const expectedCookieStr = `cookie-consent=${encodeURIComponent(JSON.stringify(consentCookieExpected))}`;
    const wrapper = shallowMount(CookieConsent, {
      localVue,
      store,
      mixins: [AnalyticsStub],
    });
    wrapper.vm.onSetCookie();
    expect(window.document.cookie).toEqual(expectedCookieStr);
  });

  it('Should create a cookie with the content of analytics false', () => {
    const consentCookieExpected = {
      essentialsCookies: true,
      analyticsCookies: false,
    };
    const expectedCookieStr = `cookie-consent=${encodeURIComponent(JSON.stringify(consentCookieExpected))}`;
    const wrapper = shallowMount(CookieConsent, {
      localVue,
      store,
      mixins: [AnalyticsStub],
    });
    wrapper.vm.analytics = false;
    wrapper.vm.onSetCookie();
    expect(window.document.cookie).toEqual(expectedCookieStr);
  });

  it('Should not render if cookie exists', () => {
    const consentCookie = {
      essentialsCookies: true,
      analyticsCookies: false,
    };
    const consentCookieStr = `cookie-consent=${encodeURIComponent(JSON.stringify(consentCookie))}`;
    Object.defineProperty(window.document, 'cookie', {
      writable: true,
      value: consentCookieStr,
    });
    const wrapper = shallowMount(CookieConsent, {
      localVue,
      store,
      propsData: {
        modalShow: false,
      },
      mixins: [AnalyticsStub],
    });
    wrapper.vm.checkIfUserAgreedToCookiePolicy();
    expect(wrapper.findComponent(BCard).attributes().hidden).toBeTruthy();
  });
});
