import { createLocalVue, shallowMount } from '@vue/test-utils';
import BootstrapVue, { BButton } from 'bootstrap-vue';
import FooterBtn from '@/components/v2/cookie-policy/FooterBtn.vue';
import { state } from '../../store/content/content.mock';

const localVue = createLocalVue();
localVue.use(BootstrapVue);

describe('Component: CookieConsent.vue', () => {
  describe('check for content', () => {
    it('Should render both buttons when Intro layer', () => {
      const content = state.content.cookiePolicy;
      const wrapper = shallowMount(FooterBtn, {
        localVue,
        propsData: {
          content,
          isIntroLayer: true,
        },
      });
      expect(wrapper.findAllComponents(BButton).length)
        .toEqual(2);
      expect(wrapper.text())
        .toContain(content.introLayer.learnMoreBtn);
      expect(wrapper.text())
        .toContain(content.introLayer.agreeBtn);
    });

    it('Should render only save button when learn more layer', () => {
      const content = state.content.cookiePolicy;
      const wrapper = shallowMount(FooterBtn, {
        localVue,
        propsData: {
          content,
          isIntroLayer: false,
        },
      });
      expect(wrapper.findAllComponents(BButton).length)
        .toEqual(1);
      expect(wrapper.text())
        .toContain(content.learnMoreLayer.agreeBtn);
    });
  });

  describe('test user interaction', () => {
    it('Should emmit learnMore when button is clicked', () => {
      const content = state.content.cookiePolicy;
      const wrapper = shallowMount(FooterBtn, {
        localVue,
        propsData: {
          content,
          isIntroLayer: true,
        },
      });
      wrapper.findAllComponents(BButton).at(0).trigger('click');
      expect(wrapper.emitted('learnMore')).toBeTruthy();
    });

    it('Should emmit setCookie when button is clicked in the intro layer', () => {
      const content = state.content.cookiePolicy;
      const wrapper = shallowMount(FooterBtn, {
        localVue,
        propsData: {
          content,
          isIntroLayer: true,
        },
      });
      wrapper.findAllComponents(BButton).at(1).trigger('click');
      expect(wrapper.emitted('setCookie')).toBeTruthy();
    });

    it('Should emmit setCookie when button is clicked in the learn more layer', () => {
      const content = state.content.cookiePolicy;
      const wrapper = shallowMount(FooterBtn, {
        localVue,
        propsData: {
          content,
          isIntroLayer: false,
        },
      });
      wrapper.findAllComponents(BButton).at(0).trigger('click');
      expect(wrapper.emitted('setCookie')).toBeTruthy();
    });
  });
});
