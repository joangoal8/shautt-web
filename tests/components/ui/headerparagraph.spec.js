import { render } from '@testing-library/vue';

import HeaderParagraph from '../../../src/components/ui/HeaderParagraph.vue';

const ANCHOR_LINK = 'https://foo/';
const ANCHOR_TEXT = 'FOO_LINK';
const ANCHOR = `<a href="${ANCHOR_LINK}">${ANCHOR_TEXT}</a>`;
const TITLE = '-title';
const FIRST_PARAGRAPH = 'FIRST';
describe('Component: VideoDemonstrationModal.vue', () => {
  describe('Behaviour', () => {
    test('It renders correctly', () => {
      const { getByText } = render(HeaderParagraph, {
        props: {
          title: TITLE,
          paragraphs: ['FIRST', ANCHOR],
        },
      });
      getByText(TITLE);
      getByText(FIRST_PARAGRAPH);
      getByText(ANCHOR_TEXT);
      expect(getByText(ANCHOR_TEXT).href)
        .toBe(ANCHOR_LINK);
    });
  });
});
