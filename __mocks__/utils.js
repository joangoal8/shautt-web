import Vue from 'vue';
import * as assets from '../src/utils/assets';

export default () => Object.defineProperty(Vue.prototype, '$utils',
  { value: { assets } });
