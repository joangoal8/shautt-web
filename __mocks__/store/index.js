import Vuex from 'vuex';

const getStore = ({ user = {} }) => new Vuex.Store({
  modules: {
    content: {},
    user: {
      namespaced: true,
      state: {
        loggedIn: false,
        errorMsg: '',
        user: {},
        postalCode: '',
      },
      mutations: {},
      actions: {},
      getters: {
        user: (state) => state.user,
        loginErrorMsg: (state) => state.errorMsg,
        isUserLogged: (state) => state.user.loggedIn,
        postalCode: (state) => state.postalCode,
      },
      ...user,
    },
    dashboard: {},
  },
  state: () => ({}),
  getters: {},
  mutations: {},
});

export default getStore;
